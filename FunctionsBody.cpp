#include <iostream>
#include "FunctionsCaption.hpp"
void Read(int& i, int& j, int matrix[I][J])
{
	std:: cout << "How many lines will there be in the matrix?" << std::endl;
	std::cin >> i;
	while (i > 100)
	{
		std::cout << "The number of lines must not exceed 100. Try again" << std::endl;
		std::cin >> i;
	}
	std::cout << "How many columns will there be in matrix?" << std::endl;
	std::cin >> j;
	while (j > 100)
	{
		std::cout << "The number of columns must not exceed 100. Try again" << std::endl;
		std::cin >> j;
	}
	std::cout << "Input " << i << "x" << j << " matrix" << std::endl;
	for (int x = 0; x < i; x++)
		for (int y = 0; y < j; y++)
		{
			std::cin >> matrix[x][y];
		}
}
bool PositiveNumber(int& i, int& j, int matrix[I][J])
{
	//для главной диагонали
	for (int x = 0; x < i; x++)
	{
		int y = x;
		if (matrix[x][y] <= 0)
			return false;
	}
	//для побочной
	for (int x = 0; x < i; x++)
	{
		int y = j - (x + 1);
		if (matrix[x][y] <= 0)
			return false;
	}
	return true;
}
bool Include_3_or_5(int& i, int& j, int matrix[I][J])
{
	//для главной
	for (int x = 0; x < i; x++)
	{
		int y = x;
		int temp = matrix[x][y];
		int count = 0;
		while (temp > 0)
		{
			if ((temp % 10 == 3) || (temp % 10 == 5))
				count += 1;
			temp /= 10;
		}
		if (count == 0)
			return false;
	}
	//для побочной
	for (int x = 0; x < i; x++)
	{
		int y = j-(x+1);
		int temp = matrix[x][y];
		int count = 0;
		while (temp > 0)
		{
			if ((temp % 10 == 3) || (temp % 10 == 5))
				count += 1;
			temp /= 10;
		}
		if (count == 0)
			return false;
	}
	return true;
}
int MinToSquare(int& i, int& j, int matrix[I][J])
{
	for (int y = 0; y < j; y++)
	{
		int minNum = INT_MAX;
		int sumColumn = 0;
		int min_i = 0;
		int min_j = 0;
		for (int x = 0; x < i; x++)
		{
			sumColumn += matrix[x][y];
			if (matrix[x][y] < minNum)
			{
				minNum = matrix[x][y];
				min_i = x;
				min_j = y;
			}
				

		}
		matrix[min_i][min_j] = sumColumn * sumColumn;

	}
	return 0;
}
void Write(int& i, int& j, int matrix[I][J])
{
	for (int x = 0; x < i; x++)
	{
		for (int y = 0; y < j; y++)
		{
			std::cout << matrix[x][y] << " ";
		}
		std::cout << std::endl;
	}
}


