﻿/*6 лабораторка 5 вариант
 Дана целочисленная матрица {Aij}i=1...n;j=1..n , n<=100. Если все диагональные элементы матрицы положительны и содержат цифры 3 или 5,
 заменить минимальные элементы столбцов на суммы квадратов элементов соответствующих столбцов.*/
#include <iostream>
#include "FunctionsCaption.hpp"
using std::cin;
using std::cout;
using std::endl;
int main()
{
	int i;
	int j;
	int matrix[I][J];
	Read(i, j, matrix);
	if (PositiveNumber(i, j, matrix) == true && Include_3_or_5(i, j, matrix) == true)
	{
		MinToSquare(i, j, matrix);
		cout << "The specified condition is met. Modified matrix is:" << endl;
		Write(i, j, matrix);
	}
	else
	{
		cout << "The specified condition is not met. The oroginal matrix is:" << endl;
		Write(i, j, matrix);
	}
	return 0;
}