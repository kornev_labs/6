#pragma once
#include <iostream>
//максимальное количество строк
const int I = 100;
//максимальное количество столбцов
const int J = 100;
//считывание матрицы
void Read(int& i, int& j, int matrix[I][J]);
//проверка на то, что бы все числа диагонали были положительными
bool PositiveNumber(int& i, int& j, int matrix[I][J]);
//проверка на наличие цифр 3 или 5 в числе
bool Include_3_or_5(int& i, int& j, int matrix[I][J]);
//поиск минимума в столбце и превращение его в квадрат суммы всех элементов столбца
int MinToSquare(int& i, int& j, int matrix[I][J]);
//напечатать матрицу
void Write(int& i, int& j, int matrix[I][J]);
